from django.http.response import HttpResponse
from django.shortcuts import render

# Create your views here.
def index(request):
    if request.method == 'POST':
        return render(request, 'confirm.html');
    return render(request, 'index.html')

def success(request):
    return render(request, 'success.html')
