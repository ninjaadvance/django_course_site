from django.test import TestCase, Client

# Create your tests here.

class IndexPageTest(TestCase):

    def test_index_page(self):
        client = Client();
        response = client.get('/liff/registration/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')
    
    def test_confirm_page(self):
        client = Client();
        response = client.post('/liff/registration/',  { 'cid': '1779800016637' })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'confirm.html')
    
    def test_success_page(self):
        client = Client();
        response = client.get('/liff/registration/success')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'success.html')