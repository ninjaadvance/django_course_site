from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='registration-index'),
    path('success', views.success, name='registration-success'),
]