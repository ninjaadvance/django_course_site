#!/bin/sh
docker-compose -f docker-compose.local.yml down
docker-compose -f docker-compose.local.yml up -d

cd e2e
yarn cypress:open --env BASE_URL=http://localhost:8000