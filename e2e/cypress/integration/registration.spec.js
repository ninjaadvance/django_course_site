describe('Registration', () => {

    it('Registration success', () => {
        // TODO: prepare user profile in the database
        cy.viewport('iphone-se2');
        cy.visit(`${Cypress.env('BASE_URL')}/liff/registration`);

        cy.contains('กรุณากรอกรหัสประชาชน');
        
        cy.get('[id="cid-input"]').type('1779800016637');
        cy.get('[id="cid-submit"]').click();

        cy.contains('นาย ประโยชน์ รุจิรา');

        cy.get('[id="confirm-button"]').click();

        cy.contains('ลงทะเบียนสำเร็จ');
        cy.get('[id="close"]').should('be.visible');
        // TODO: check line id in the database 
    });

    xit('User not found', () => {});
    
});
